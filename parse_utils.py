"""
Library with some natural language and other parsing utilities.
"""

# basic libraries
import ast
import re

def join_list(li,map_with=str,filter_with=bool):
    """
    Join a list of things.
    Adds commas and the word "and".
    """
    li = list(map(map_with,filter(filter_with,li)))
    n = len(li)
    if n == 0:
        return ''
    elif n == 1:
        return li[0]
    elif n == 2:
        return li[0]+' and '+li[1]
    else:
        return ', '.join(li[:-1])+', and '+li[-1]

def tokens(istr):
    """
    If this string was command line arguments,
    what would the arguments be?
    Words are kept as words.
    String literals become their evaluated form.
    """
    while istr:
        if istr[0]=='"':
            match = re.search('"[^"\\\\]*(?:\\\\.[^"\\\\]*)*"',istr)
            if not match:
                raise ValueError('Could not find a closing quote in the text')
            to = match.end()
            yield ast.literal_eval(istr[:to])
            istr = istr[to:]
        else:
            match = re.search('\\s+',istr)
            if match:
                to = match.start()
            else:
                to = len(istr)
            yield istr[:to]
            istr = istr[to:]
        if istr:
            match = re.search('\\s+',istr)
            if not match or match.start() != 0:
                raise ValueError('Separator must be a space')
            istr = istr[match.end():]

class subcommand(object):
    """
    Base class for subcommands.
    """
    async def act(self, context):
        """
        Attempt to do this subcommand.
        Either perform the action and return True,
        or do nothing and return False.
        """
        return False

class command_handler(object):
    """
    Base class for command handlers.
    """
    async def handle(self, context):
        """
        Handle some command.
        Tokens come from context.tokens.
        Override the other methods, not this.
        """
        await self.start(context)
        context.sub = None
        context.args = []
        for token in context.tokens:
            context.token = token
            subs = await self.subs(context)
            ignore = set()
            if hasattr(context, 'ignore'):
                ignore |= set(context.ignore or ())
            if token in ignore:
                continue
            if token in subs:
                if context.sub:
                    if not await context.sub.act(context):
                        raise ValueError('subcommand could not be understood with these parameters')
                    context.args = []
                context.sub = subs[token]
            else:
                context.args.append(token)
        if context.sub:
            if not await context.sub.act(context):
                raise ValueError('subcommand could not be understood with these parameters')
            context.sub = None
            context.args = []
        await self.end(context)
    async def check_leftovers(self, context):
        if context.sub or context.args:
            if context.sub:
                raise ValueError('subcommand could not be understood with these parameters')
            raise ValueError('parameters without a subcommand')
    async def start(self, context):
        """
        Called at the start of handle
        """
        pass
    async def end(self, context):
        """
        Called at the end of handle
        """
        pass
    async def subs(self, context):
        """
        Get all current subcommands as a dict
        mapping name to the object.
        """
        return {}