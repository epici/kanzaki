"""
Functional utilities and classes.
"""

class reverse_order(object):
    """
    Object wrapper with reversed comparison order.
    """
    def __init__(self, value):
        self.value = value
    def __lt__(self, other):
        return self.value > other.value
    def __le__(self, other):
        return self.value >= other.value
    def __gt__(self, other):
        return self.value < other.value
    def __ge__(self, other):
        return self.value <= other.value
    def __eq__(self, other):
        return self.value == other.value
    def __ne__(self, other):
        return self.value != other.value

def chain_functions(funcs):
    """
    Apply each of the functions in order.
    """
    funcs = list(funcs)
    def chained_function(x):
        for func in funcs:
            x = func(x)
        return x
    return chained_function

def join_sort_keys(keys,reverse=None):
    """
    Turn multiple sort keys into a single lexicographic sort key.
    """
    keys = list(keys)
    if reverse:
        try:
            reverse = list(reverse)
            if len(reverse) != len(keys):
                raise ValueError('reverse length must be same as keys length')
        except TypeError:
            reverse = [True] * len(keys)
        for i in range(len(reverse)):
            if reverse[i]:
                keys[i] = chain_functions((keys[i],reverse_order))
    def joined_keys(x):
        result = []
        for key in keys:
            result.append(key(x))
        return tuple(result)
    return joined_keys