"""
Makes saving and loading data easier.
"""

def load(filename, _globals=None):
    """
    Attempt to load data from a file.
    """
    _globals = _globals or globals()
    try:
        with open(filename,'r',encoding='utf-8') as file:
            data = file.read()
        exec(data, _globals)
    except FileNotFoundError as err:
        print(err)

def save(filename, names):
    """
    Attempt to write the values of the given variables to a file.
    """
    lines = []
    for name in names:
        try:
            lines.append(name+' = '+repr(globals()[name]))
        except KeyError as err:
            lines.append('#'+name+' missing')
            print(err)
    data = '\n'.join(lines)
    with open(filename,'w',encoding='utf-8') as file:
        file.write(data)

def log(code, filename=None, _globals=None):
    """
    Execute some code, and write the code executed to a file.
    """
    _globals = _globals or globals()
    exec(code, _globals)
    if filename:
        with open(filename,'a',encoding='utf-8') as file:
            file.write('\n'+code)