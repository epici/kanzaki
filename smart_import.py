"""
This library enhances import functionality
by installing missing libraries
and hashing local libraries.
"""

# basic standard libraries
import sys
import os
import subprocess

# get hash library
import quick_hash

library_hashes = set()

def library_hash(filename=None,module=None,get=False):
    """
    Optionally add a library's hash to the list.
    Optionally get a hash of the list, representing all libraries.
    """
    if module:
        filename = module.__file__
    if filename:
        library_hashes.add(quick_hash.hash_file(filename))
    if get:
        return quick_hash.hash_string(b''.join(sorted(map(lambda x:x.digest(32),library_hashes))))

def import_module(name=None,rename=None,pip_name=None,add_hash=False,_globals=None):
    """
    First attempts to import name as rename.
    On fail, attempts to pip3 install pip_name.
    If add_hash is set, will hash the file. Used for local libraries.
    """
    _globals = _globals or globals()
    rename = rename or name
    try:
        _globals[rename] = module = __import__(name, _globals, locals(), [], 0)
    except ImportError as err:
        pip_name = pip_name or name
        if not pip_name:raise err
        subprocess.run(['pip3','install',pip_name])
        _globals[rename] = module = __import__(name, _globals, locals(), [], 0)
    if add_hash:
        library_hash(module=module)

library_hash(filename='smart_import.py')
library_hash(filename='quick_hash.py')