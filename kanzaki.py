"""
Kanzaki: Puzzles and secrets bot
"""

# get importer
import smart_import

# import builtin libraries
import datetime
import re
import operator
import traceback
import functools
import string

# import outside libraries
smart_import.import_module(name='discord',pip_name='discord.py',_globals=globals())

# import local libraries
smart_import.import_module(name='quick_hash',add_hash=True,_globals=globals())
smart_import.import_module(name='parse_utils',add_hash=True,_globals=globals())
smart_import.import_module(name='io_utils',add_hash=True,_globals=globals())
smart_import.import_module(name='func_utils',add_hash=True,_globals=globals())
smart_import.import_module(name='discord_utils',add_hash=True,_globals=globals())
smart_import.library_hash(filename=__file__)

def pseudoword(convert, seed):
    """
    Return a string of the same form as convert.
    X or x are replaced with consonants.
    Y or y are replaced with vowels.
    """
    xs = 'bcdfghjklmnprstvz'
    ys = 'aeiou'
    if type(seed) == bytes:
        seed = int.from_bytes(seed, 'little')
    num_x = convert.lower().count('x')
    num_y = convert.lower().count('y')
    base_x = len(xs)
    base_y = len(ys)
    mod_x = base_x ** num_x
    mod_y = base_y ** num_y
    seed %= mod_x*mod_y
    build = []
    for char in convert:
        if char in 'Xx':
            seed, v = divmod(seed, base_x)
            v = xs[v]
        elif char in 'Yy':
            seed, v = divmod(seed, base_y)
            v = ys[v]
        else:
            v = char
        if char.isupper():
            v = v.upper()
        build.append(v)
    return ''.join(build)

# get version hash
version_hash = smart_import.library_hash(get=True).digest(32)
version_hash = pseudoword('Xyxy Xyxxyx', version_hash)

class dummy(object):
    """
    Class for one-off objects
    """
    def __init__(self,**kwargs):
        self.__dict__.update(kwargs)

def str_datetime(x):
    """
    Return a readable string representation of a datetime object
    """
    x = x.replace(microsecond=0)
    return str(x)

def str_timedelta(x):
    """
    Return a readable string representation of a timedelta object
    """
    x = datetime.timedelta(days=x.days, seconds=x.seconds)
    return str(x)

def set_all(coll, value, keys):
    """
    Set all keys to map to this value in coll.
    """
    for key in keys:
        coll[key] = value

class puzzle_solve(object):
    """
    Represents a user's solve for a puzzle.
    """
    def __init__(self, who=None, when=None):
        self.who = who
        self.when = when
    def __repr__(self):
        return 'puzzle_solve('+', '.join(name+'='+repr(getattr(self,name)) for name in ['who', 'when'])+')'

class puzzle_remove(object):
    """
    Represents a user's removal of a puzzle.
    """
    def __init__(self, who=None, when=None):
        self.who = who
        self.when = when
    def __repr__(self):
        return 'puzzle_remove('+', '.join(name+'='+repr(getattr(self,name)) for name in ['who', 'when'])+')'

class puzzle(object):
    """
    Represents a puzzle.
    """
    @staticmethod
    def charset_of(istr):
        if len(istr) == 0:
            raise ValueError('Cannot detect the chararacter set of nothing')
        max_digit = -1
        max_letter = -1
        has_lower = False
        has_upper = False
        has_space = False
        has_dash = False
        has_underscore = False
        istr = set(istr)
        try:
            istr = set(map(ord,istr))
        except:
            pass # already in ord'd format
        istr = sorted(istr, reverse=True)
        for i in istr:
            if 48<=i<=57: # digit
                max_digit = max(max_digit, i-48)
            elif 97<=i<=122: # lowercase
                max_letter = max(max_letter, i-97)
                has_lower = True
            elif 65<=i<=90: # uppercase
                max_letter = max(max_letter, i-65)
                has_upper = True
            elif 32==i:
                has_space = True
            elif 45==i:
                has_dash = True
            elif 95==i:
                has_underscore = True
            else:
                raise ValueError('This character is not in any known character set: '+chr(i))
        case_text = (
            '',
            'lowercase ',
            'uppercase ',
            'mixed case '
            )[has_upper*2+has_lower]
        parts = []
        if max_digit != -1 and max_letter != -1:
            # digits and letters
            if max_letter <= 5:
                parts.append('hexadecimal')
            else:
                parts.append('letters')
                parts.append('numbers')
        elif max_letter != -1:
            # letters
            parts.append('letters')
        elif max_digit != -1:
            # digits
            if max_digit <= 1:
                parts.append('binary')
            elif max_digit <= 7:
                parts.append('octal')
            else:
                parts.append('numbers')
        if has_space:
            parts.append('spaces')
        if has_dash:
            parts.append('dashes')
        if has_underscore:
            parts.append('underscores')
        result = case_text + parse_utils.join_list(parts)
        return result
    @staticmethod
    def unwrap(istr):
        match = re.match('(\\w*)\\((.+?)\\)(\\w*)',istr)
        if match:
            groups = match.groups()
            return groups[1], groups[0]+groups[2]
        return istr, ''
    def __init__(self, created_by=None, created_at=None, name_short=None, name_long=None, description=None, solution=None, tags=None, solved_by=None, remove=None):
        self.created_by = created_by
        self.created_at = created_at
        self.name_short = name_short
        self.name_long = name_long
        self.description = description
        self.solution = solution
        self.tags = tags or []
        self.solved_by = solved_by or []
        self.remove = remove
        self.usolution, self.wrapping = puzzle.unwrap(solution)
        self.charset = puzzle.charset_of(self.usolution)
    def __repr__(self):
        return 'puzzle('+', '.join(name+'='+repr(getattr(self,name)) for name in ['created_by', 'created_at', 'name_short', 'name_long', 'description', 'solution', 'tags', 'solved_by', 'remove'])+')'
    async def is_removed(self):
        """
        Is this puzzle removed?
        """
        return self.remove is not None
    async def solve_index(self, uid):
        """
        Get the solve index and object for this user.
        If they did not solve this puzzle, returns None.
        """
        for i,x in enumerate(self.solved_by):
            if x.who == uid:
                return i,x
        return None
# discord client object
client = discord.Client()

# quque logout
logout = False

# traceback info for each channel
traceback_details = {}

# load basic info
# expected values:
me = None
#    me.id as string
#    me.secret as string
#    me.token as string
ids = None
#    ids.bots as set of strings
#    ids.admins as set of strings
ratelimits = None
#    rate_limits.server as float
#    rate_limits.channel as float
#    rate_limits.user as float
colours = None
#    colours.normal as discord.Colour
#    colours.error as discord.Colour
io_utils.load('local/kanzaki_info.py', _globals=globals())
discord_utils.embed_colours.update(colours.__dict__) # add all colours
me.prefix = '<@'+me.id+'> ' # prefix is mention

# puzzles database
puzzles = dummy()
puzzles.by_name = {}
puzzles.by_solution = {}
def add_puzzle(ipuzzle):
    """
    Add a puzzle to the database
    """
    puzzles.by_name[ipuzzle.name_short] = ipuzzle
    puzzles.by_solution[ipuzzle.usolution] = ipuzzle
def remove_puzzle(ipuzzle,iremove):
    """
    Decommission a puzzle, partially removing it from the database
    """
    if type(ipuzzle)==str:
        ipuzzle = puzzles.by_name[ipuzzle]
    del puzzles.by_solution[ipuzzle.usolution]
    ipuzzle.name_long = '(removed)'
    ipuzzle.description = ''
    ipuzzle.solution = ipuzzle.usolution = ''
    ipuzzle.tags = []
    ipuzzle.wrapping = ''
    ipuzzle.charset = ''
    ipuzzle.remove = iremove
def solved_puzzle(ipuzzle,isolve):
    if type(ipuzzle)==str:
        ipuzzle = puzzles.by_solution[ipuzzle]
    ipuzzle.solved_by.append(isolve)
# load puzzles history
io_utils.load('local/puzzles.py', _globals=globals())
# possible sort keys
puzzles.sort_by = {}
set_all(puzzles.sort_by, lambda context:lambda x:x.name_short, [
    'id',
    'shorthand',
    'shortcut',
    ])
set_all(puzzles.sort_by, lambda context:lambda x:x.name_long, [
    'title',
    ])
set_all(puzzles.sort_by, lambda context:lambda x:x.created_at, [
    'time',
    'date',
    ])
set_all(puzzles.sort_by, lambda context:func_utils.chain_functions((lambda x:x.created_by, functools.partial(discord_utils.get_user_name, server=context.message.server))), [
    'creator',
    'author',
    ])
set_all(puzzles.sort_by, lambda context:lambda x:len(x.solved_by), [
    'solves',
    'solved',
    ])
set_all(puzzles.sort_by, lambda context:func_utils.chain_functions((lambda x:x.tags[0], ['toy','normal','professional','future'].index)), [
    'difficulty',
    'quality',
    ])
set_all(puzzles.sort_by, lambda context:func_utils.chain_functions((lambda x:x.tags[1], ['no-tools','pencil-and-paper','calculator','computer'].index)), [
    'tools',
    'toolset',
    ])

# load description texts
describe_texts = {}
io_utils.load('local/describe_texts.py', _globals=globals())

# load common words
common_words = set()
try:
    with open('local/common_words.txt','r') as file:
        for line in file:
            common_words.add(line.strip())
except FileNotFoundError as err:
    print(err)

# create rate limit trackers
last_used = dummy()
last_used.server = {}
last_used.channel = {}
last_used.user = {}

def context_setter(context, name, value=None, ignore=None, mapwith=None):
    """
    For command handlers, this sets a value in a context.
    """
    _ignore = ignore
    async def act(context):
        items = list(context.args)
        ignore = set(_ignore or ())
        if ignore:
            items = list(filter(lambda x:x not in ignore,items))
        if len(items) == 1:
            ivalue = items[0]
            if mapwith:
                ivalue = mapwith(ivalue)
            setattr(context, name, ivalue)
            return True
        elif len(items) == 0 and value is not None:
            setattr(context, name, value)
            return True
        return False
    return act

async def check_rate(message):
    """
    Check if a message complies with rate limits.
    """
    current_time = datetime.datetime.utcnow()
    checks = [
        [message.channel.id, last_used.channel, rate_limits.channel],
        [message.author.id, last_used.user, rate_limits.user],
        ]
    if message.server:
        # DM has no server
        checks.append([message.server.id, last_used.server, rate_limits.server])
    for uid, lu, rl in checks:
        if uid in lu and (current_time - lu[uid]).total_seconds() < rl:
            return False
    return True

async def update_rate(message):
    """
    Update last_used dict for a message.
    """
    current_time = datetime.datetime.utcnow()
    checks = [
        [message.channel.id, last_used.channel],
        [message.author.id, last_used.user],
        ]
    if message.server:
        # DM has no server
        checks.append([message.server.id, last_used.server])
    for uid, lu in checks:
        lu[uid] = current_time

class command_handler_logout(parse_utils.command_handler):
    async def end(self, context):
        global logout
        await self.check_leftovers(context)
        if context.message.author.id in ids.admins:
            logout = True
        else:
            raise ValueError('You need to be an admin to do this')

class command_handler_guess(parse_utils.command_handler):
    async def end(self, context):
        guesses = context.tokens
        correct = []
        seen = set()
        for guess in guesses:
            guess = puzzle.unwrap(guess)[0]
            if guess in seen:continue
            seen.add(guess)
            try:
                puzzle.charset_of(guess)
            except ValueError:
                raise ValueError('Your guess "'+guess+'" could not possibly be a solution')
            if guess in puzzles.by_solution:
                ipuzzle = puzzles.by_solution[guess]
                if ipuzzle.created_by == context.message.author.id:continue
                if await ipuzzle.solve_index(context.message.author.id):continue
                correct.append(guess)
        num_guesses = len(guesses)
        num_correct = len(correct)
        description = 'I found '+str(num_guesses)+' '+('guess','guesses')[num_guesses != 1]+', and '
        if correct:
            description += str(len(correct))+' '+('was','were')[num_correct != 1]+' correct:'
            for guess in correct:
                ipuzzle = puzzles.by_solution[guess]
                description += '\n('+guess+') for `'+ipuzzle.name_short+'`'
                isolve = puzzle_solve(who=context.message.author.id, when=context.current_time)
                io_utils.log('solved_puzzle('+repr(guess)+', '+repr(isolve)+')',filename='local/puzzles.py',_globals=globals())
        else:
            description += 'none were correct.'
        embed = await discord_utils.make_embed(colour='normal',title='Results of your guesses',description=description)
        await client.send_message(context.message.channel, embed=embed)

class command_handler_create(parse_utils.command_handler):
    async def start(self, context):
        context.ignore = {
            'as',
            'for',
            }
    async def end(self, context):
        await self.check_leftovers(context)
        if not hasattr(context, 'name_short'):
            raise ValueError('No short name was provided')
        if not hasattr(context, 'name_long'):
            raise ValueError('No long name was provided')
        if not hasattr(context, 'description'):
            raise ValueError('No description was provided')
        if not hasattr(context, 'solution'):
            raise ValueError('No solution was provided')
        if not hasattr(context, 'difficulty'):
            raise ValueError('No difficulty/quality was provided')
        if not hasattr(context, 'tools'):
            raise ValueError('No toolset was specified')
        ipuzzle = puzzle(
            created_by = context.message.author.id,
            created_at = context.current_time,
            name_short = context.name_short,
            name_long = context.name_long,
            description = context.description,
            solution = context.solution,
            tags = [context.difficulty, context.tools]
            )
        if not 3<=len(ipuzzle.name_short)<=12:
            raise ValueError('Short name is required to be between 3 and 12 characters long to be easily typeable')
        if set(ipuzzle.name_short.lower()) - set(string.ascii_lowercase+string.digits+'-'):
            raise ValueError('Short name can only contain letters, numbers, and dashes')
        if ipuzzle.name_short.lower() in common_words:
            raise ValueError('Short name cannot be one of the most common English words')
        if not 3<=len(ipuzzle.name_long)<=80:
            raise ValueError('Long name is required to be between 3 and 80 characters long to be easily readable')
        if set(ipuzzle.name_long) & set('*_~`'):
            raise ValueError('Long name cannot contain Markdown symbols')
        if any(map(lambda x:x<' ',ipuzzle.name_long)):
            raise ValueError('Long name cannot contain line breaks or control characters')
        if not 3<=len(ipuzzle.usolution)<=80:
            raise ValueError('Solution is required to be between 3 and 80 characters long, at least 6 to be less guessable and at most 80 to be typeable')
        if ipuzzle.usolution.lower() in common_words:
            raise ValueError('Solution cannot be one of the most common English words')
        if not 12<=len(ipuzzle.description)<=500:
            raise ValueError('Description is required to be between 12 and 500 characters long, at least 12 to provide enough information, and at most 500 to not be huge')
        if ipuzzle.name_short in puzzles.by_name:
            raise ValueError('Short name is already taken')
        if ipuzzle.usolution in puzzles.by_solution:
            raise ValueError('Another puzzle already has this solution')
        io_utils.log('add_puzzle('+repr(ipuzzle)+')',filename='local/puzzles.py',_globals=globals())
        embed = await discord_utils.make_embed(colour='normal',title='Created puzzle '+ipuzzle.name_short,description='I\'ll take care of the rest. Check your puzzle, it should be up now.')
        await client.send_message(context.message.channel, embed=embed)
    async def subs(self, context):
        result = {}
        setter = functools.partial(context_setter, context)
        alias_to = functools.partial(set_all, result)
        alias_to(dummy(act=setter('_',1)),[
            'and',
            'with',
            'using',
            'puzzle',
            'riddle',
            'challenge',
            ])
        if not hasattr(context, 'name_short'):
            alias_to(dummy(act=setter('name_short')),[
                'id',
                'shorthand',
                'shortcut',
                ])
        if not hasattr(context, 'name_long'):
            alias_to(dummy(act=setter('name_long')),[
                'title',
                ])
        if not hasattr(context, 'name_short') or not hasattr(context, 'name_long'):
            async def act(context):
                if len(context.args) == 2:
                    if context.args[0] == 'short' and not hasattr(context, 'name_short'):
                        context.name_short = context.args[1]
                        return True
                    elif context.args[0] == 'long' and not hasattr(context, 'name_long'):
                        context.name_long = context.args[1]
                        return True
                    elif context.args[1] == 'short' and not hasattr(context, 'name_short'):
                        context.name_short = context.args[0]
                        return True
                    elif context.args[1] == 'long' and not hasattr(context, 'name_long'):
                        context.name_long = context.args[0]
                        return True
                return False
            alias_to(dummy(act=act),[
                'name',
                ])
        if not hasattr(context, 'description'):
            alias_to(dummy(act=setter('description')),[
                'description',
                'text',
                'info',
                'information',
                ])
        if not hasattr(context, 'solution'):
            alias_to(dummy(act=setter('solution')),[
                'solution',
                'solve',
                'answer',
                ])
        if not hasattr(context, 'difficulty'):
            alias_to(dummy(act=setter('difficulty','toy')),[
                'toy',
                'demo',
                'demonstration',
                'tutorial',
                ])
            alias_to(dummy(act=setter('difficulty','normal')),[
                'normal',
                'regular',
                'standard',
                ])
            alias_to(dummy(act=setter('difficulty','professional')),[
                'professional',
                'paid',
                ])
            alias_to(dummy(act=setter('difficulty','future')),[
                'future',
                'academic',
                ])
        if not hasattr(context, 'tools'):
            alias_to(dummy(act=setter('tools','no-tools')),[
                'no-tools',
                'toolless',
                'mental',
                ])
            alias_to(dummy(act=setter('tools','pencil-and-paper')),[
                'pencil-and-paper',
                'pen-and-paper',
                'pencil',
                'pen',
                'paper',
                'writing',
                'memory',
                'space',
                'drawing',
                'notes',
                'notepad',
                'notebook',
                ])
            alias_to(dummy(act=setter('tools','calculator')),[
                'calculator',
                'math',
                ])
            alias_to(dummy(act=setter('tools','computer')),[
                'computer',
                'pc',
                'application',
                'applications',
                'app',
                'apps',
                'script',
                'scripts',
                'scripting',
                ])
        return result

class command_handler_view(parse_utils.command_handler):
    async def start(self, context):
        context.ignore = {
            'at',
            'the',
            'puzzle',
            'puzzles',
            }
        context.page = None
        context.sort_by = None
    async def end(self, context):
        if context.page or context.sort_by:
            # view page of puzzles
            await self.check_leftovers(context)
            page_size = 15
            puzzles_list = list(puzzles.by_name.values())
            for i in range(len(puzzles_list))[::-1]:
                x = puzzles_list[i]
                if await x.is_removed():
                    del puzzles_list[i]
            num_puzzles = len(puzzles_list)
            num_pages = (num_puzzles-1)//page_size+1
            num_pages = max(1, num_pages)
            page = context.page or 1
            if page>0:page -= 1
            page = page%num_pages
            sort_by = context.sort_by
            if sort_by is not None:
                puzzles_list.sort(key=sort_by)
            puzzles_list = puzzles_list[page*page_size:(page+1)*page_size]
            puzzles_text = []
            for x in puzzles_list:
                puzzles_text.append('`'+x.name_short+'` **'+x.name_long+'** created by '+(await discord_utils.get_user(x.created_by,client=client,server=context.message.server)).display_name+' at '+str_datetime(x.created_at))
            embed = await discord_utils.make_embed(colour='normal', title='Viewing page '+str(page+1)+' of '+str(num_pages)+', '+str(page_size)+' per page', description='\n'.join(puzzles_text))
            await client.send_message(context.message.channel, embed=embed)
        elif len(context.args) == 1:
            # view single puzzle
            name = context.args[0]
            if name not in puzzles.by_name:
                raise ValueError('No puzzle has the short name `'+name+'`')
            ipuzzle = puzzles.by_name[name]
            is_removed = await ipuzzle.is_removed()
            title = 'Puzzle `'+ipuzzle.name_short+'`'
            description = '**'+ipuzzle.name_long+'**\nCreated by '+(await discord_utils.get_user(ipuzzle.created_by,client=client,server=context.message.server)).display_name+' at '+str_datetime(ipuzzle.created_at)+'\n\n'+(ipuzzle.description if not is_removed else 'Removed by '+(await discord_utils.get_user(ipuzzle.remove.who,client=client)).display_name+' at '+str_datetime(ipuzzle.remove.when))
            embed = await discord_utils.make_embed(colour='normal', title=title, description=description)
            if not is_removed:
                embed.add_field(name='Hints', value='Wrapping: '+(ipuzzle.wrapping or 'none')+'\nCharacter set: '+ipuzzle.charset)
                embed.add_field(name='Tags', value=parse_utils.join_list(ipuzzle.tags))
            if ipuzzle.solved_by:
                num_solves = len(ipuzzle.solved_by)
                solved_list = list(enumerate(ipuzzle.solved_by[0:5]))
                solve_self = await ipuzzle.solve_index(context.message.author.id)
                if solve_self and solve_self[0]>=5:
                    solved_list.append(solve_self)
                for j,(i,x) in list(enumerate(solved_list)):
                    solved_list[j] = str(i+1)+'. '+(await discord_utils.get_user(x.who,client=client,server=context.message.server)).display_name+' at '+str_datetime(x.when)+' ('+str_timedelta(x.when-ipuzzle.created_at)+' after puzzle creation)'
                embed.add_field(name='Solved by '+str(num_solves)+' '+('person','people')[num_solves!=1], value='\n'.join(solved_list))
            await client.send_message(context.message.channel, embed=embed)
        else:
            raise ValueError('You need to tell me what you want to view')
    async def subs(self, context):
        result = {}
        setter = functools.partial(context_setter, context)
        alias_to = functools.partial(set_all, result)
        if not context.page:
            async def act(context):
                if len(context.args) == 1:
                    context.page = int(context.args[0])
                    if context.page == 0:
                        raise ValueError('There is no 0th page, please be clear about which page number you want')
                    return True
                return False
            alias_to(dummy(act=act), [
                'page',
                ])
            alias_to(dummy(act=setter('page',1)), [
                'all',
                'first',
                ])
        if not context.sort_by:
            async def act(context):
                found = list(context.args)
                if found:
                    if found[0] in {'by','with','using'}:
                        found = found[1:]
                    can_then = False
                    reverse = False
                    keys = []
                    reverse_list = []
                    for name in found:
                        if name == 'then':
                            if can_then:
                                can_then = False
                                continue
                            raise ValueError('You are using the word "then" wrong')
                        if name in {'reverse','reversed','backward','descending'}:
                            if reverse:
                                raise ValueError('Don\'t tell me to reverse it twice, that\'s confusing')
                            reverse = True
                            continue
                        if name not in puzzles.sort_by:
                            raise ValueError('I do not know how to sort by '+found)
                        keys.append(puzzles.sort_by[name](context))
                        reverse_list.append(reverse)
                        can_then = True
                        reverse = False
                    if keys:
                        if len(set(keys)) != len(keys):
                            raise ValueError('Your sorting method contains a duplicate item')
                        context.sort_by = func_utils.join_sort_keys(keys, reverse_list)
                        return True
                return False
            alias_to(dummy(act=act), [
                'sort',
                'sorted',
                'order',
                'ordered',
                'arrange',
                'arranged',
                'group',
                'grouped',
                ])
        return result
    
class command_handler_remove(parse_utils.command_handler):
    async def end(self, context):
        if len(context.args) == 0:
            raise ValueError('Tell me what you want me to remove')
        if len(context.args) > 1:
            raise ValueError('You can only remove one puzzle at a time')
        name = context.args[0]
        if name not in puzzles.by_name:
            raise ValueError('There is no puzzle with the short name `'+name+'`')
        ipuzzle = puzzles.by_name[name]
        if ipuzzle.created_by != context.message.author.id and context.message.author.id not in ids.admins:
            raise ValueError('You cannot remove other people\'s puzzles')
        if ipuzzle.solved_by and context.message.author.id not in ids.admins:
            raise ValueError('A puzzle cannot be removed after it is solved')
        if await ipuzzle.is_removed():
            raise ValueError('This puzzle has already been removed')
        iremove = puzzle_remove(who=context.message.author.id, when=context.current_time)
        io_utils.log('remove_puzzle('+repr(name)+', '+repr(iremove)+')', filename='local/puzzles.py', _globals=globals())
        embed = await discord_utils.make_embed(colour='normal', title='Removed puzzle `'+name+'`', description='Note that removed puzzles remain in the database and their name cannot be used again.')
        await client.send_message(context.message.channel, embed=embed)

class command_handler_describe(parse_utils.command_handler):
    async def end(self, context):
        items = list(filter(lambda x:x not in {'what','is','are','a','an','the'},context.args))
        if len(items) > 1:
            raise ValueError('I can only describe one thing at a time')
        name = items[0] if items else ''
        if name not in describe_texts:
            raise ValueError('I do not know what '+name+' is')
        text = describe_texts[name]
        replace = {
            'ME':(context.message.server.me.name if context.message.server else client.user.display_name),
            'YOU':context.message.author.display_name,
            }
        text = re.sub('|'.join(replace), lambda x:replace[x.group(0)], text)
        title, description = text.split('\n---\n')
        embed = await discord_utils.make_embed(colour='normal', title=title, description=description)
        await client.send_message(context.message.channel, embed=embed)

class command_handler_traceback(parse_utils.command_handler):
    async def end(self, context):
        if context.message.author.id not in ids.admins:
            raise ValueError('You must be an admin to see error logs')
        tb = traceback_details.get(context.message.channel.id, '')
        embed = await discord_utils.make_embed(colour='normal', title='Last exception', description='```py\n'+tb+'\n```')
        await client.send_message(context.message.channel, embed=embed)

# map keywords to command_handler constructor or function
commands = {}
register_command = functools.partial(set_all, commands)
register_command(command_handler_logout,[
    'die',
    'kill',
    'logout',
    ])
register_command(command_handler_guess,[
    'answer',
    'attempt',
    'check',
    'guess',
    'submit',
    'try',
    ])
register_command(command_handler_create,[
    'build',
    'create',
    'make',
    ])
register_command(command_handler_view,[
    'look',
    'see',
    'view',
    ])
register_command(command_handler_remove,[
    'remove',
    'delete',
    ])
register_command(command_handler_describe,[
    'describe',
    'explain',
    'what',
    'help',
    ])
register_command(command_handler_traceback,[
    'traceback',
    'exception',
    'error',
    ])

@client.event
async def on_ready():
    # notify on login
    print('='*40)
    print('Logged in as '+client.user.name+'#'+str(client.user.discriminator)+'\n\tID '+client.user.id)
    if client.user.id != me.id:
        print('ID mismatch. Info file says '+me.id)
    print('Current time is '+str(datetime.datetime.utcnow()))
    print('='*40)
    # update game for first time
    await client.change_presence(game=discord.Game(name='beta build '+version_hash))

@client.event
async def on_message(message):
    global traceback_details
    embed = None
    if message.content.startswith(me.prefix) and await check_rate(message):
        command_text = message.content[len(me.prefix):]
        try:
            tokens = list(parse_utils.tokens(command_text))
            command = tokens[0]
            tokens = tokens[1:]
        except ValueError as err:
            command = None
            traceback_short = str(err)
            traceback_details[message.channel.id] = traceback.format_exc(5)
            embed = await discord_utils.make_embed(colour='error',title='Something went wrong',description=traceback_short)
        if command not in commands:
            embed = await discord_utils.make_embed(colour='error',title='Unknown command',description='I don\'t know what to do with this, rephrase it and try again?')
        else:
            handler = commands[command]
            await update_rate(message)
            handler = handler()
            context = dummy()
            context.current_time = datetime.datetime.utcnow()
            context.message = message
            context.tokens = tokens
            try:
                await handler.handle(context)
            except Exception as err:
                traceback_short = str(err)
                traceback_details[message.channel.id] = traceback.format_exc(5)
                embed = await discord_utils.make_embed(colour='error',title='Something went wrong',description=traceback_short)
    if embed:
        await client.send_message(message.channel, embed=embed)
    if logout:
        print('='*40)
        print('Logging out now...')
        print('='*40)
        await client.logout()

# run the bot
try:
    client.run(me.token)
except:
    pass