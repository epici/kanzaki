"""
Adds some shortcuts on top of the discord library.
"""

import discord

embed_colours = {}

async def make_embed(**kwargs):
    """
    Create a embed object with the desired properties.
    """
    if 'colour' in kwargs:
        col = kwargs['colour']
        if col in embed_colours:
            col = embed_colours[col]
        if type(col)==str:
            col = int(col.split('#')[-1].split('x')[-1], base=16)
        if type(col)==int:
            col = discord.Colour(col)
        kwargs['colour'] = col
    result = discord.Embed(**kwargs)
    return result

users = {}

async def get_user(uid=None,client=None,server=None):
    """
    Get a user for a given ID.
    If the user is not cached, use the client to fetch it.
    If a server is given, will try to get the corresponding Member object.
    """
    if uid not in users:
        users[uid] = await client.get_user_info(uid)
    result = server and server.get_member(uid) or users[uid]
    return result

def get_user_name(uid=None,server=None):
    """
    Attempt to get the name of a particular user.
    If a server is given, will try to use their nickname instead.
    On fail, return the user's ID prefixed with a null character.
    """
    if uid not in users:
        return '\0'+uid
    result = server and server.get_member(uid) or users[uid]
    return result.display_name