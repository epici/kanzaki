"""
Simplifies hashing data.
All hash methods return a hash_type object so
as much digest as needed can be extracted.
"""

# need for hashing
import hashlib

def floor_log2(x):
    """
    For a regular positive integer,
    return the floor of the base 2 logarithm of it.
    """
    return len(bin(x))-3

# make hash type variable
hash_type = hashlib.shake_128

# calculate preferred block size
ideal_block_size = 1<<16
block_size = hash_type().block_size
block_size <<= max(0,floor_log2(ideal_block_size)-floor_log2(block_size)-1)

def digest64(h):
    """
    Return the first 64 bits of digest as an integer.
    Uses little endian byte order.
    """
    return int.from_bytes(h.digest(8),'little')

def hash_string(x):
    """
    Returns the hash of a string or bytes object.
    Strings are converted using utf-8.
    """
    h = hash_type()
    try:
        h.update(x)
    except TypeError:
        x = x.encode('utf-8')
        h.update(x)
    return h

def hash_file(x):
    """
    Given a file name, get the hash of its contents.
    """
    h = hash_type()
    with open(x,'rb') as file:
        buf = file.read(block_size)
        while buf:
            h.update(buf)
            buf = file.read(block_size)
    return h